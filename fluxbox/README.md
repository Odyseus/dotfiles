Fluxbox
=====================

# Original Theme Screenshot

## Exilorate Style

### Window

![Window Screenshot][ss-exilorate-window]

### Menu

![Menu Screenshot][ss-exilorate-menu]

### SVG Source

SVG available freely.

![SVG Slices][ss-exilorate-svg]

-- -- --

# Old Screenshot

<strong>OS</strong>: Arch<br/>
<strong>WM</strong>: Fluxbox<br/>
  + blackarch menu in fluxbox<br/>
  + dockbarx<br/>
  + tmux [ncmpcpp play, ncmpcpp viz] in xfce-terminal<br/>
  + fb-theme: axonkolor (manual modification)<br/>
  + conky bar (manual modification)<br/>
<br/>
::.. Glowing Flux  ..::

![Fluxbox Screenshot][picasa-ss-fluxbox-old]


[ss-exilorate-window]:   https://gitlab.com/epsi-rns/dotfiles/raw/master/fluxbox/styles/exilorate/fluxbox-exilorate-window.png
[ss-exilorate-menu]:     https://gitlab.com/epsi-rns/dotfiles/raw/master/fluxbox/styles/exilorate/fluxbox-exilorate-menu.png
[ss-exilorate-svg]:      https://gitlab.com/epsi-rns/dotfiles/raw/master/fluxbox/styles/exilorate/exilorate.png

[picasa-ss-fluxbox-old]: https://lh3.googleusercontent.com/-U6rIIOLyMLg/Vz2oBSAABUI/AAAAAAAAARM/WXU-K0lwXyAJ9PC-mgky7vOO0LV23STYQCCo/s0/fluxbox-glow.png
